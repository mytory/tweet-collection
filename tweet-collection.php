<?php
/**
 * Plugin Name: Tweet collection
 * Description: This plugin collect tweets. tweets` post_type is ‘tweet’, when you save tweets general post and tweet do not mixed.
 * Author: An, HyeongWoo
 * Author URI: https://mytory.net
 * Version: 1.1.10
 */

class MytoryTweetCollection
{
    public $tweet_collection_plugin_worked = false;

    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'tweet_collection_init']);
        add_action('init', [$this, 'register_custom_post_type']);
        add_action('admin_menu', [$this, 'top_menu']);

        /**
         * 백업에서 임포트
         */
        add_action('admin_menu', [$this, 'admin_menu']);

        if ( ! $this->is_setup_complete()) {
            add_action('admin_notices', [$this, 'should_setup_msg']);
        }

        // 20분에 한 번씩 실행되는 옵션을 cron 에 등록한다. (나중엔 얼마에 한 번씩 긁어올 지도 설정할 수 있게 한다.)
        add_filter('cron_schedules', [$this, 'cron_add_20m']);

        add_action('collect_tweets', [$this, 'collect_and_create_posts']);

        $this->check_cron();

        register_activation_hook(__FILE__, [$this, 'tweet_collection_activate']);
        register_deactivation_hook(__FILE__, [$this, 'tweet_collection_deactivate']);

        add_action('the_title', [$this, 'remove_my_username_from_title'], 1);
        add_filter('single_post_title', [$this, 'remove_my_username_from_title'], 1);

        if (get_option('tweet-collection-title-length') and get_option('tweet-collection-title-length') !== 0) {
            add_action('the_title', [$this, 'apply_title_length']);
            add_filter('single_post_title', [$this, 'apply_title_length']);
        }

        add_filter('archive_template', [$this, 'rss_template']);
    }

    /**
     * Get all option names.
     *
     * @return string[]
     */
    function get_option_names(): array
    {
        return [
            'tweet-collection-twitter-username',
            'tweet-collection-title-length',
            'tweet-collection-number-tweets',
            'tweet-collection-consumer-key',
            'tweet-collection-consumer-secret',
            'tweet-collection-access-token',
            'tweet-collection-access-token-secret',
        ];
    }

    /**
     * From option name to variable name
     *
     * @param $opt_name
     *
     * @return String
     */
    function convert_var_name($opt_name): string
    {
        $var_name = str_replace('tweet-collection-', '', $opt_name);

        return str_replace('-', '_', $var_name);
    }

    /**
     * Register language file.
     *
     */
    function tweet_collection_init()
    {
        load_plugin_textdomain('tweet-collection', false, dirname(plugin_basename(__FILE__)).'/languages');
    }


    /**
     * Register custom post type
     *
     */
    function register_custom_post_type()
    {
        $labels = [
            'name'               => _x('Tweets', 'post type general name', 'tweet-collection'),
            'singular_name'      => _x('Tweet', 'post type singular name', 'tweet-collection'),
            'add_new'            => _x('Add New', 'tweet', 'tweet-collection'),
            'add_new_item'       => __('Add New Tweet', 'tweet-collection'),
            'edit_item'          => __('Edit Tweet', 'tweet-collection'),
            'new_item'           => __('New Tweet', 'tweet-collection'),
            'all_items'          => __('All Tweets', 'tweet-collection'),
            'view_item'          => __('View Tweet', 'tweet-collection'),
            'search_items'       => __('Search Tweets', 'tweet-collection'),
            'not_found'          => __('No tweets found', 'tweet-collection'),
            'not_found_in_trash' => __('No tweets found in Trash', 'tweet-collection'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Tweets', 'tweet-collection'),

        ];
        $args   = [
            'labels'              => $labels,
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'has_archive'         => true,
            'hierarchical'        => false,
            'menu_position'       => 4,
            'exclude_from_search' => true,
            'taxonomies'          => ['post_tag'],
            'supports'            => [
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'custom-fields',
                'revisions',
            ],
        ];
        register_post_type('tweet', $args);
    }

    /**
     * 옵션 페이지 html, action 등록.
     *
     */
    function top_menu()
    {
        add_options_page(__('Tweet Collection', 'tweet-collection'), __('Tweet Collection', 'tweet-collection'),
            'manage_options', 'tweet-collection', [$this, 'menu_page']);
    }

    /**
     * 옵션 페이지 함수
     *
     */
    function menu_page()
    {
        if ( ! current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'tweet-collection'));
        }

        $option_names = $this->get_option_names();

        if ( ! empty($_POST)) {
            foreach ($option_names as $option) {
                $var_name = $this->convert_var_name($option);
                update_option($option, $_POST[$var_name]);
            }
            $message = __('Saved!', 'tweet-collection');

            // 옵션 저장하면 트윗을 한 번 긁어 와 준다.
            do_action('collect_tweets');
        }

        if (isset($_GET['delete_all_settings']) && 'y' == $_GET['delete_all_settings']) {
            $this->delete_settings();
        }

        include 'tc-menu-page.php';
    }


    /**
     * @param  mixed  $path
     */
    function get_objects_from_backup_json($path)
    {
        $tmp     = explode("\n", file_get_contents($path));
        $tmp[0]  = '[ {';
        $content = implode("\n", $tmp);
        $objects = json_decode($content);
        if ( ! $objects) {
            wp_die('백업 js 파일이 잘못됐습니다. 파일을 열어서 올바른 json 파일로 만들어 주세요.');
        }

        return $objects;
    }

    function get_objects_from_like_backup_json($path)
    {
        $objects = array_reverse(json_decode(file_get_contents($path)));
        if ( ! $objects) {
            wp_die('백업 js 파일이 잘못됐습니다. 파일을 열어서 올바른 json 파일로 만들어 주세요.');
        }

        return $objects;
    }

    /**
     * @param  mixed  $objects
     */
    function import_from_backup($objects)
    {
        foreach ($objects as $obj) {
            $tweet        = $obj->tweet;
            $post_content = $this->get_post_content($tweet->full_text, $tweet->entities->urls);
            $post_title   = htmlspecialchars_decode(strip_tags($post_content));
            $tweet_guid   = 'https://twitter.com/mytory/statuses/'.$tweet->id_str;
            if ($this->is_already_insert($tweet_guid) > 0) {
                echo "<p>$tweet_guid already imported.</p>";
                continue;
            }
            $this->insert_post_and_post_meta($tweet, $post_content, $tweet_guid, $post_title);
            ?>
            <p><?php
                echo esc_html($post_content); ?> imported.</p>
            <?php
            flush();
        }

        echo '<p>완료</p>';
    }

    /**
     * @param  mixed  $objects
     */
    function import_from_like_backup($objects)
    {
        foreach ($objects as $obj) {
            $like        = $obj->like;

            $post_content = !empty($like->fullText) ? $this->make_links_clickable($like->fullText) : $like->expandedUrl;
            $post_title   = htmlspecialchars_decode(strip_tags($post_content));
            $tweet_guid   = $like->expandedUrl;
            if ($this->is_already_insert($tweet_guid) > 0) {
                echo "<p>$tweet_guid already imported.</p>";
                continue;
            }
            $post_id = $this->insert_post_and_post_meta_from_like($like, $post_content, $tweet_guid, $post_title);
            update_post_meta($post_id, 'tweet_type', 'like');

            // 'like' 태그가 있는지 검사
            if (!term_exists('like', 'post_tag')) {
                // 'like' 태그가 없으면 생성
                $like_tag = wp_insert_term('like', 'post_tag');
                if (is_wp_error($like_tag)) {
                    $wp_error = $like_tag;
                    echo 'error: ' . $wp_error->get_error_code() . ' ' . implode(' / ', $wp_error->get_error_messages());
                }

                // 생성된 'like' 태그의 ID를 가져옴
                $like_tag_id = $like_tag['term_id'];
            } else {
                $like_tag = get_term_by('name', 'like', 'post_tag');
                $like_tag_id = $like_tag->term_id;
            }

            // $post에 'like' 태그를 추가
            wp_set_post_terms($post_id, array($like_tag_id), 'post_tag', true);

            ?>
            <p><?php
                echo esc_html($post_content); ?> imported.</p>
            <?php
            flush();
        }

        echo '<p>완료</p>';
    }

    /**
     * @param $tweet
     * @param  string  $post_content
     * @param  string  $tweet_guid
     * @param  string  $post_title
     *
     * @return int|WP_Error
     */
    function insert_post_and_post_meta($tweet, string $post_content, string $tweet_guid, string $post_title)
    {
        $gmt_offset    = get_option('gmt_offset');
        $datetime      = $this->get_datetime($tweet->created_at, $gmt_offset);
        $post_date_gmt = $this->get_post_date_gmp($tweet->created_at, $gmt_offset);
        $args          = [
            'comment_status' => 'closed',
            'post_date'      => $post_date_gmt,
            'ping_status'    => 'closed',
            'post_content'   => $post_content
                ."\n\n<a class='tweet-permalink' href='$tweet_guid' title='Tweet Permalink'>$datetime</a>",
            'post_status'    => 'publish',
            'post_title'     => $post_title,
            'post_type'      => 'tweet',
        ];
        $post_id       = wp_insert_post($args);

        if ($post_id) {
            update_post_meta($post_id, 'tweet_guid', $tweet_guid);
            update_post_meta($post_id, 'tweet_type', 'tweet');
        }

        return $post_id;
    }

    /**
     * @param Object $like
     * @param string $post_content
     * @param string $tweet_guid
     * @param string $post_title
     * @return int|WP_Error
     */
    function insert_post_and_post_meta_from_like(Object $like, string $post_content, string $tweet_guid, string $post_title)
    {
        $args          = [
            'comment_status' => 'closed',
            'ping_status'    => 'closed',
            'post_content'   => $post_content
                ."\n\n<a class='tweet-permalink' href='$tweet_guid' title='Tweet Permalink'>Permlink</a>",
            'post_status'    => 'publish',
            'post_title'     => $post_title,
            'post_type'      => 'tweet',
        ];
        $post_id       = wp_insert_post($args);

        if ($post_id) {
            update_post_meta($post_id, 'tweet_guid', $tweet_guid);
            update_post_meta($post_id, 'tweet_id', $like->tweetId);
            update_post_meta($post_id, 'tweet_type', 'like');
        }

        return $post_id;
    }

    /**
     * 트위터 아이디 설정을 하지 않은 경우 등록하라고 메시지를 뿌린다.
     *
     */
    function should_setup_msg()
    {
        ?>
        <div class="updated">
            <p>
                <?php
                _e('Set options completely for Tweet Collection.', 'tweet-collection'); ?>
                <!--suppress HtmlUnknownTarget -->
                <a href="options-general.php?page=tweet-collection"><?php
                    _e('Go to Tweet Collection Option page!', 'tweet-collection'); ?></a>
            </p>
        </div>
        <?php
    }

    /**
     * @param  mixed  $schedules
     */
    function cron_add_20m($schedules)
    {
        // Adds once weekly to the existing schedules.
        $schedules['20m'] = [
            'interval' => 60 * 20,
            'display'  => __('Once by 20 minutes', 'tweet-collection'),
        ];

        return $schedules;
    }

    /**
     * 설정값이 모두 제대로 들어가 있는지 검사한다.
     *
     */
    function is_setup_complete(): bool
    {
        $tc_option_names = $this->get_option_names();
        foreach ($tc_option_names as $opt_name) {
            if ( ! get_option($opt_name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 트위터의 json 을 긁어서 집어 넣을 콘텐츠를 만든다.
     *
     * @return array
     */
    function get_timeline(): array
    {
        require_once 'twitteroauth/twitteroauth/twitteroauth.php'; // Path to twitteroauth library

        $twitter_username    = get_option('tweet-collection-twitter-username');
        $number_tweets       = get_option('tweet-collection-number-tweets');
        $consumer_key        = get_option('tweet-collection-consumer-key');
        $consumer_secret     = get_option('tweet-collection-consumer-secret');
        $access_token        = get_option('tweet-collection-access-token');
        $access_token_secret = get_option('tweet-collection-access-token-secret');

        $connection = $this->get_connection_with_access_token($consumer_key, $consumer_secret, $access_token,
            $access_token_secret);

        return $connection->get('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.$twitter_username
            .'&count='.$number_tweets);
    }

    /**
     * @param  mixed  $cons_key
     * @param  mixed  $cons_secret
     * @param  mixed  $oauth_token
     * @param  mixed  $oauth_token_secret
     */
    function get_connection_with_access_token($cons_key, $cons_secret, $oauth_token, $oauth_token_secret): TwitterOAuth
    {
        return new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
    }

    /**
     * 트위터의 xml 을 긁어 와서 새 Tweet Post Type 으로 등록하는 함수를 만든다.
     *
     */
    function collect_and_create_posts(): bool
    {
        // 설정이 제대로 안 돼 있으면 중단.
        if ( ! $this->is_setup_complete()) {
            return false;
        }

        // 작동중이라면 중복 작동하지 않도록 한다.
        if ($this->tweet_collection_plugin_worked) {
            return false;
        }

        $this->tweet_collection_plugin_worked = true;

        $timeline = $this->get_timeline();

        foreach ($timeline as $tweet) {
            $post_content = $this->get_post_content($tweet->text, $tweet->entities->urls);
            $post_title   = htmlspecialchars_decode(strip_tags($post_content));
            $tweet_guid   = 'https://twitter.com/mytory/statuses/'.$tweet->id_str;
            if ($this->is_already_insert($tweet_guid) > 0) {
                continue;
            }
            $this->insert_post_and_post_meta($tweet, $post_content, $tweet_guid, $post_title);
        }

        return true;
    }

    /**
     * Text 에 있는 URL을 실제 URL 로 변경한다.
     *
     * @param  string  $text
     * @param  array  $urls  URL 정보가 있는 배열
     *
     * @return string t.co URL text 를 실제 URL text 와 HTML 링크로 변경한 문자열
     */
    function get_post_content(string $text, array $urls): string
    {
        foreach ($urls as $url_info) {
            $a_tag = "<a href='$url_info->expanded_url'>$url_info->display_url</a>";
            $text  = str_replace($url_info->url, $a_tag, $text);
        }

        return $text;
    }

    function make_links_clickable($text)
    {
        // 좌측 <a.*?</a>(*SKIP)(*F)| 는 이미 링크가 붙은 태그 안을 건드리지 않게 하는 것.
        /** @noinspection PhpArgumentWithoutNamedIdentifierInspection */
        $text = preg_replace(
            '!<a(.|\n)*?</a>(*SKIP)(*F)|(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я가-힣0-9@:%_+.~#?&;/=]+)!i',
            '<a href="$2">$2</a>',
            $text
        );

        // http:// 없이 시작하는 링크 처리.
        // ".과 공백 제외한 문자가 이어진 뒤 .으로 끝나는 것"이 1-3회 반복되고 위 tld 중 하나로 끝나면 URL 로 인식한다.
        $tld_list_regex = implode('|', ['com', 'kr', 'org', 'net', 'uk', 'gov', 'edu', 'info', 'biz', 'asia', 'mil', 'cn', 'jp', 'us']);
        return preg_replace(
            '!<a(.|\n)*?</a>(*SKIP)(*F)|(([^. ]+\.){1,3}(' . $tld_list_regex . ')/[-a-zA-Zа-яА-Я가-힣()0-9@:%_+.~#?&;/=]+)!i',
            '<a href="http://$2">$2</a>',
            $text
        );
    }


    /**
     * @param  mixed  $created_at
     * @param  mixed  $gmt_offset
     */
    function get_post_date_gmp($created_at, $gmt_offset)
    {
        return date('Y-m-d H:i:s', $gmt_offset * 60 * 60 + strtotime($created_at));
    }

    /**
     * @param  mixed  $created_at
     * @param  mixed  $gmt_offset
     */
    function get_datetime($created_at, $gmt_offset): string
    {
        $timestamp = $gmt_offset * 60 * 60 + strtotime($created_at);

        return date_i18n(get_option('date_format').' '.get_option('time_format'), $timestamp);
    }

    /**
     * 중복 컨텐츠 확인을 위한 함수
     *
     * @param  mixed  $tweet_guid
     */
    function is_already_insert($tweet_guid): int
    {
        $query = new WP_Query('meta_key=tc_tweet_guid&meta_value='.$tweet_guid
            .'&post_type=tweet&post_status=publish,future');

        return $query->post_count;
    }


    /**
     * 플러그인 활성화할 때 wp_cron 등록
     *
     */
    function tweet_collection_activate()
    {
        wp_schedule_event(time(), '20m', 'collect_tweets');
    }

    /**
     * cron 등록 여부를 확인하고 등록이 해제돼 있으면 재등록.
     */
    function check_cron()
    {
        if ( ! wp_get_schedule('collect_tweets')) {
            wp_schedule_event(time(), '20m', 'collect_tweets');
        }
    }


    /**
     * 플러그인 비활성화할 때 wp_cron 해제
     *
     */
    function tweet_collection_deactivate()
    {
        wp_clear_scheduled_hook('collect_tweets');
    }

    /**
     * @param  mixed  $text
     * @param  mixed  $len
     */
    function text_dot($text, $len): string
    {
        $text = strip_tags($text);
        if (strlen($text) <= $len) {
            return $text;
        } else {
            $text = wp_specialchars_decode($text);
            $text = mb_substr($text, 0, $len, 'utf-8');
            $text = esc_html($text);

            return $text.'…';
        }
    }

    /**
     * 트윗 제목에서 내 ID 제거
     *
     * @param  mixed  $title
     */
    function remove_my_username_from_title($title)
    {
        if (get_post_type() == 'tweet') {
            $username        = get_option('tweet-collection-twitter-username');
            $username_length = mb_strlen($username);
            if (mb_substr($title, 0, $username_length + 2) == $username.': ') {
                return mb_substr($title, $username_length + 2);
            } else {
                return $title;
            }
        }

        return $title;
    }


    /**
     * 트윗 제목 길이
     *
     * @param  mixed  $title
     */
    function apply_title_length($title)
    {
        if (get_post_type() == 'tweet') {
            $title_length = get_option('tweet-collection-title-length');

            return $this->text_dot($title, $title_length);
        }

        return $title;
    }


    /**
     * @param  mixed  $archive_template
     */
    function rss_template($archive_template)
    {
        global $wp_query;
        if (is_post_type_archive('tweet') and isset($_GET['rss']) and $_GET['rss'] == 'for-fb') {
            $args             = [
                'post_type'      => 'tweet',
                'posts_per_page' => 20,
            ];
            $wp_query         = new WP_Query($args);
            $archive_template = dirname(__FILE__).'/rss.php';
        }

        return $archive_template;
    }


    /**
     * If user want, delete settings.
     */
    function delete_settings()
    {
        $option_names = $this->get_option_names();
        foreach ($option_names as $option) {
            delete_option($option);
        }
    }

    /**
     * 백업에서 임포트
     * @return void
     */
    function admin_menu()
    {
        add_management_page(
            __('Import Tweets from Backup', 'tweet-collection'),
            __('Import Tweets from Backup', 'tweet-collection'),
            'manage_options',
            'import-from-backup',
            function () {
                if ( ! empty($_FILES['backup_file']['tmp_name'])) {
                    set_time_limit(-1);
                    $objects = $this->get_objects_from_backup_json($_FILES['backup_file']['tmp_name']);
                    $this->import_from_backup($objects);
                }
                require 'tc-import-from-backup.php';
            }
        );

        add_management_page(
            __('Import Like Tweets from Backup', 'tweet-collection'),
            __('Import Like Tweets from Backup', 'tweet-collection'),
            'manage_options',
            'import-from-like-backup',
            function () {
                if ( ! empty($_FILES['backup_file']['tmp_name'])) {
                    set_time_limit(-1);
                    $objects = $this->get_objects_from_like_backup_json($_FILES['backup_file']['tmp_name']);
                    $this->import_from_like_backup($objects);
                }
                require 'tc-import-from-like-backup.php';
            }
        );
    }
}

require_once 'widget.php';
require_once 'functions.php';

$tweet_collection_obj = new MytoryTweetCollection();
