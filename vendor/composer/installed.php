<?php return array(
    'root' => array(
        'name' => 'mytory/tweet-collection',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'e058904fba3301d52416dab1af5f5ea3e54035f3',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'erusev/parsedown' => array(
            'pretty_version' => '1.7.4',
            'version' => '1.7.4.0',
            'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../erusev/parsedown',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'mytory/tweet-collection' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'e058904fba3301d52416dab1af5f5ea3e54035f3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
